"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime
import json
import requests


class RawAPI:
    @staticmethod
    def get_json_data(url, auth=[]):
        response = requests.get(url, auth=auth)

        if response.ok:
            return json.loads(response.content.decode("utf-8"))
        else:
            response.raise_for_status()
            return []


class BaseAPI(RawAPI):
    BASE_URL = ""
    DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
    FIELD_CREATION_DATE = ""
    FIELD_ID = ""
    NAME = "Unknown"

    @staticmethod
    def check(extra):
        pass

    @classmethod
    def get_apps(cls):
        return []

    @classmethod
    def get_app_by_id(cls, app_id):
        return {}

    @classmethod
    def get_app_by_record(cls, record):
        return {}

    @classmethod
    def get_downloads_for_date(cls, year="", month="", day=""):
        return {}

    @classmethod
    def parse_date(cls, date):
        formats = cls.DATETIME_FORMAT

        if type(formats) is not list:
            formats = [formats]

        for f in formats:
            try:
                return datetime.strptime(date, f)
            except ValueError:
                continue

        raise ValueError("time data %s does not match formats %s"
                         % (date, formats))


from .flathub import FlathubAPIv1
from .github import GithubFlathubAPI
